# vim

This vim configuration features a vim-python workflow:
- fzf, ripgrep & tagbar for file/project navigation/finding
- Fugitive for git
- ALE for language server protocol (pylsp & vimint)
- REPL integration to [i]python3 and jupyter-qtconsole
- send lines to terminal/ipython mappings
- everforest & solarized colorschemes (corrected for tmux with CSExact)
- set transparent background after loading any theme (with aftercolors)
- tmux status bar with current folder gitstats (gitmux)
- tmux seamless clipboard between vim sessions and system clipboard (xclip)
- plugins installed through git.submodules

# Some usage
```
    `hjkl`    : Navigate splits
    `h l`     : Navigate tabs
    <F8>      : vsplit file structure (tagbar)
    <leader>/ : fuzzy find words in buffers (fzf)
    <Ctrl>F   : fuzzy find files from cwd tree (fzf)
    <leader>f : fuzzy grep words within files from cwd tree (ripgrep)
    <leader>b : fuzzy select buffers
# REPL
    :call StartREPL() : activate the REPL plugin
    <leader>r         : open vim-terminal with loaded python virtual environment 
    <leader>w         : send text object to python interpreter
# Terminal
    <leader>x : send line(s) to terminal
    <F2>        : terminal enter visual mode
# tmux:
    Prefix mapped to `Ctrl-A`
    `Ctrl-A` +v +h : split horizontal vertical
    Navigate panes with `C-hjklz`
    Navigate windows with `C-n[ext]p[re vius]`
```

# Installing
## Linux (tested on Debian Bookworm)
``` 
# install packages
sudo apt install

    # tested terminals
    xfce4-terminal
    gnome-terminal
    # get everforest colorscheme from https://gogh-co.github.io/Gogh/

    # recomended vim version
    vim-gtk3 # gvim +python3 (has('gui_running') using gvim for some ale completions)
    # not recomended, alternatives
    vim      # -python3 (no jupyter-vim, no clipboard)
    vim-nox  # terminal +python3

    # integrated apps
    fzf ripgrep universal-ctags xclip tmux

    # python virtual environment required
    python3-venv

# make vim default editor
sudo update-alternatives --config editor

# make a python virtual environment for the language server protocol
mkdir -p ~/venv
python3 -m venv --system-site-packages ~/venv/vim
echo "alias venvim='source ~/venv/vim/bin/activate'">>~/.bashrc
bash
venvim
python -m pip install --upgrade pip setuptools wheel

# clone this
git clone --recurse-submodules git@gitlab.com:fdobad/dotvim.git ~/.vim
pip install -r ~/.vim/requirements.txt

# setup _files
cp -b _tmux.conf ~/.tmux.conf
cat _bashrc>>~/.bashrc
``` 
Optional part: gitmux for displaying git status in current directory
```
    mkdir ~/.tmux/plugins/gitmux
    # donwload the latest binary release from
    # https://github.com/arl/gitmux/releases
    cd ~/.tmux/plugins/gitmux
    tar -xzf gitmux_0.7.10_linux_amd64.tar.gz
```
If you don't want it remove around this line from `.tmux.conf`
```
    set -g status-right '#(~/.tmux/plugins/gitmux/gitmux "#{pane_current_path}")'
```
## Windows (gvim 32bit)
Get the binary from releases : [gvim-32bit]( https://github.com/vim/vim-win32-installer/releases )
```TODO
    winget python32bits (copy id)
    git clone git@gitlab.com:fdobad/dotvim.git ~/vimfiles
```

# Plugins-Submodules included
Check `.gitmodules` for details

    majutsushi/tagbar
    junegunn/fzf.vim
    tpope/vim-fugitive

    sainnhe/everforest
    altercation/vim-colors-solarized
    KevinGoodsell/vim-csexact

    dense-analysis/ale
    github/copilot

    roxma/vim-tmux-clipboard
    fdobad/vim-repl

To update plugins: `git submodule update --remote`

To add: `git submodule add --name jupyter git@github.com:jupyter-vim/jupyter-vim.git ~/.vim/pack/plugin/[start|opt]/jupyter-vim`

Update help:

    :helptags ALL
    $vim -c ':helptags ~/.vim/path/to/new/plugin/doc/'

# To Do
0. Fonts not following Appearance in gvim (xfce4):

        if has("gui_running")
          if has("gui_gtk2")
            set guifont=Inconsolata\ 12
          elseif has("gui_macvim")
            set guifont=Menlo\ Regular:h14
          elseif has("gui_win32")
            set guifont=Consolas:h11:cANSI
          endif
        endif


1. Jupyter plugin is not currently on the submodules, but a sane config is in `~/.vim/ftplugins/python.vim`:
- add to start
- pip install jupyter-qtconsole

2. Try:

        https://github.com/untitled-ai/jupyter_ascending
        git@github.com:jimeh/tmux-themepack
        https://github.com/tmux-plugins/tmux-continuum  
        https://github.com/mattpetters/tmux-statusline-themes  

# test
latest commit : 30c327a 
