" Python ftplugin
" 
let g:python_highlight_all = 1
setlocal tabstop=4 softtabstop=4 shiftwidth=4 smarttab expandtab autoindent fileformat=unix
setlocal foldmethod=indent
let g:virtualenv_directory = '~/pyenvs'

" ALE {{{1

"let g:ale_python_pylint_options='--max-line-length 120'
"let g:ale_python_flake8_options='--max-line-length=120'
"let b:ale_linters_explicit = 1
"let b:ale_linters_ignore = ['pycodestyle', 'pyls', 'pylint', 'pyright', 'ruff'] "default 'python': ['flake8', 'mypy', 'pylint', 'pyright', 'ruff']
"let b:ale_fixers = ['black', 'isort', 'remove_trailing_lines', 'trim_whitespace']
"let b:ale_echo_msg_format = '[%linter%] %s [%severity%]'
"let g:ale_virtualtext_cursor = 0
"let b:ale_completion_enabled = 1
"let b:ale_lint_delay = 500
"let b:ale_lint_on_text_changed = 'normal'
"let b:ale_floating_window_border = ['│', '─', '╭', '╮', '╯', '╰', '│', '─'] " [] " repeat([''], 8)
"let g:ale_lint_on_text_changed = 'never'
"let g:ale_lint_on_enter = 0
"let g:ale_lint_on_save = 0
"
"let b:ale_fix_on_save=1
"
"let g:ale_python_auto_pipenv = 1
"let g:ale_python_auto_virtualenv = 1
"let g:ale_python_flake8_auto_pipenv = 1
"let g:ale_python_mypy_auto_pipenv = 1

"nnoremap <F9> <Plug>(ale_lint)
"nnoremap <F10> <Plug>(ale_go_to_definition_in_split)
"nnoremap <F11> <Plug>(ale_hover)
"nnoremap <F12> <Plug>(ale_toggle)
"imap <C-p> <Plug>(ale_complete)
" monos
"[I]nformational messages that Pylint emits (do not contribute to your analysis score)
"[R]efactor for a 'good practice' metric violation
"[C]onvention for coding standard violation
"[W]arning for stylistic problems, or minor programming issues
"[E]rror for important programming issues (i.e. most probably bug)
"[F]atal for errors which prevented further processing
"let g:ale_sign_info = '❕'
"let g:ale_sign_warning = '❕'
"let g:ale_sign_error = '❗'
"let g:ale_sign_warning = '🐝'
"let g:ale_sign_error = '🐞'
"let g:ale_sign_offset = 1000000
"let g:ale_sign_style_error = '❗'
"let g:ale_sign_style_warning = '❕'


"function! s:on_lsp_buffer_enabled() abort
"
"  setlocal omnifunc=lsp#complete
"  "setlocal omnifunc=ale#completion#OmniFunc
"
"  setlocal signcolumn=yes
"  if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
"  nmap <buffer> gd <plug>(lsp-definition)
"  nmap <buffer> gs <plug>(lsp-document-symbol-search)
"  nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
"  nmap <buffer> gr <plug>(lsp-references)
"  nmap <buffer> gi <plug>(lsp-implementation)
"  nmap <buffer> gt <plug>(lsp-type-definition)
"  nmap <buffer> <leader>rn <plug>(lsp-rename)
"  nmap <buffer> [g <plug>(lsp-previous-diagnostic)
"  nmap <buffer> ]g <plug>(lsp-next-diagnostic)
" nmap <buffer> K <plug>(lsp-hover)
"  "nnoremap <buffer> <expr><c-f> lsp#scroll(+4)
"  "nnoremap <buffer> <expr><c-d> lsp#scroll(-4)
"  "ALEPopulateQuickfix
"
"  "autocmd! BufWritePre call execute('LspDocumentFormatSync')
"  
"  "set foldmethod=expr
"  "  \ foldexpr=lsp#ui#vim#folding#foldexpr()
"  "  \ foldtext=lsp#ui#vim#folding#foldtext()
"endfunction
"
"augroup lsp_install
"    au!
"    " call s:on_lsp_buffer_enabled only for languages that has the server registered.
"    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
"augroup END

" ONCE ?
":LspInstallServer
"  install-pyls-all.cmd          
"  install-pyls-all.sh           
"  install-pyls.cmd              
"  install-pyls-ms.cmd           
"  install-pyls-ms.sh     <- python-language-server old
"  install-pylsp-all.cmd         
"  install-pylsp-all.sh   <- python-lsp-server :)
"  install-pylsp.cmd             
"  install-pylsp.sh              
"  install-pyls.sh               
"  install-pyright-langserver.cmd
"  install-pyright-langserver.sh 

" Vim-REPL {{{1
function StartREPL()
	packadd vim-repl
	" https://github.com/sillybun/vim-repl
	" w: send line, visual selection
	"       def, class, while, for, if (g:repl_auto_sends)
	" r: start/stop console
	nnoremap <leader>r :REPLToggle<Cr>
	" e: hide console
	nnoremap <leader>e :REPLHide<Cr>
	" s: send block # BEGIN ... # END
	nnoremap <leader>s :REPLSendSession<Cr>
	" d: send right hand side
	nnoremap <leader>d :REPLSendRHSofCurrentLine<Cr>
	let g:repl_console_name = 'REPLterm'
	let g:repl_python_automerge = 1
	let g:repl_code_block_fences = {'python': '# -', 'markdown': '```'}
	let g:repl_code_block_fences_end = {'python': '# +', 'markdown': '```'}
	let g:repl_program = { 'python': 'ipython3' , 'python-debug' : 'ipdb' }
	"let g:repl_program = { 'python': 'jupyter-console' , 'python-debug' : 'ipdb' }
	nnoremap <F12> <Esc>:REPLDebugStopAtCurrentLine<Cr>
	nnoremap <F10> <Esc>:REPLPDBN<Cr>
	nnoremap <F11> <Esc>:REPLPDBS<Cr>
	"let g:repl_predefine_python = { 'numpy': 'import numpy as np', 
	" \ 'matplotlib': 'from matplotlib import pyplot as plt',
	" \ 'logging' : 'import logging as log',
	" \ 'log' : 'log.basicConfig(force=True, level=10)'
	" \ }
  " set virtual environment
  " else by adding #REPLENV: /home/fdo/venv on your file
	if has('win32')
		let g:repl_python_pre_launch_command="/OSGeo4W/bin/ipython-setup-qgis.bat"
		" ipython-setup-qgis.bat is the same as python-qgis.bat without the last line
	endif
endfunc

" jupyter-vim {{{1
function StartJupyter()
	packadd jupyter-vim
	:JupyterConnect<Cr>
	let g:jupyter_mapkeys = 0
	nnoremap <buffer> <silent> <localleader>e :JupyterSendRange<CR>
 	"let g:vim_virtualenv_path = '/home/fdo/Source/py3env'
 	"let g:jupyter_kernel_type= 'python'
  let g:vim_virtualenv_path = '/home/fdo/pyenv/qgis'
  "if exists('g:vim_virtualenv_path')
  "" TODO doit without activate_this.py
  "    pythonx import os; import vim
  "    pythonx activate_this = os.path.join(vim.eval('g:vim_virtualenv_path'), 'bin/activate_this.py')
  "    pythonx with open(activate_this) as f: exec(f.read(), {'__file__': activate_this})
  "endif
endfunc
"autocmd Filetype python call JupyterVim() 

" Jupytext {{{1
" needs jupytext python package
"function JupyText()
" nnoremap <F5> :w<CR> :execute('!jupytext --to md '.expand('%f'))<CR>	" convert notebook.ipynb to a .py file
"	map <F6> :!jupytext --to notebook notebook.py              " convert notebook.py to an .ipynb file with no outputs
"	map <F11> :!jupytext --to notebook --execute jupytext_example.md    " convert notebook.md to an .ipynb file and run it
"	map <F10> :!jupytext --update --to notebook notebook.py    " update the input cells in the .ipynb file and preserve outputs and metadata
"	map <F9> :!jupytext --set-formats ipynb,py notebook.ipynb  " Turn notebook.ipynb into a paired ipynb/py notebook
"	map <F12> :!jupytext --sync notebook.ipynb                 " Update all paired representations of notebook.ipynb
"endfunc
