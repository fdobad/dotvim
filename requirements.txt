# python-lsp-server[pylint]
python-lsp-ruff
python-lsp-server
pylsp-mypy
pylsp-rope
python-lsp-isort
python-lsp-black

ipython               
pickleshare # ipython
qtconsole # qgis ipython console

vim-vint
