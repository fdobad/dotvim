" Enable syntax highlighting @/usr/share/vim/vim90/syntax
syntax on

" Enable filetype detection, load plugin & indent rules.vim
" :he filetype-overview
filetype plugin indent on

nnoremap H ^
nnoremap L $
vnoremap <leader>y "+y
vnoremap <leader>p "+gP

" Colorscheme
" DARK/LIGHT
set background=light "dark
" set background=light "dark
" truecolor 24bit colors 
" https://github.com/termstandard/colors
if has('termguicolors')
  set termguicolors
endif

" Tmux 
if exists('$TMUX')
  " load before setting colorscheme
  packadd csexact
  " share clipboard across vim sessions 
  " needs (apt install) xclip
  packadd vim-tmux-clipboard
  " Sets the default clipboard to the system clipboard
  if has('clipboard')
  	set clipboard=unnamed
  	if has('unnamedplus')
  		set clipboard=unnamedplus,autoselect,exclude:cons\|linux " Sets the default clipboard to the system clipboard
  	endif
  endif
  " Easy paste line 
  "  option 1: paste visual selection into the last tmux pane
	vnoremap  <leader>y y :execute('!tmux paste-buffer -p -t {last} \; send-keys -t {last} Enter Enter') <CR><CR>
  "  option 2: doesn't need tmux-clipboard plugin
  " vmap <leader>; :w! ~/vim.tmp<CR> :!tmux set-buffer "$(cat ~/vim.tmp)"<CR> :execute '!tmux select-pane -l \; paste-buffer -p \; send-keys Enter Enter \; select-pane -l'<CR><CR>
  " rename tmux window with buffer title
  " deprecated? depends on vim-tmux-focus-events plugin for FocusGained/Lost in terminal
	"autocmd BufReadPost,FileReadPost,BufNewFile,FocusGained,WinEnter * call system("tmux rename-window " . expand("%:t"))
	"autocmd VimLeave,FocusLost * call system("tmux set-window-option automatic-rename")
endif

" Colorscheme 
" EVERFOREST 
function Everforest(background, contrast)
  packadd everforest
  " https://github.com/sainnhe/everforest
  let g:everforest_disable_terminal_colors = 0 " disable changin :term
  let g:everforest_background = a:background   " 'hard', 'medium'(default), 'soft'
  let g:everforest_ui_contrast = a:contrast    " 'low', 'high'
  let g:transparent_background = 1             " 0, 1
  let g:everforest_transparent_background = 1  " 0, 1, 2
  let g:everforest_better_performance = 0      " if slow loading
  colorscheme everforest
endfunc
call Everforest('soft', 'high')
" SOLARIZED 
function Solarized()
  "packadd csexact
  packadd solarized
  " https://github.com/altercation/vim-colors-solarized
  " let g:solarized_termtrans=0
  " let g:solarized_degrade=1
  let g:solarized_bold=0
  " let g:solarized_underline=1
  " let g:solarized_italic=1
  let g:solarized_termcolors=256
  " let g:solarized_contrast='normal' " 'high' or 'low'
  " let g:solarized_visibility='normal' " 'high' or 'low'
  " let g:solarized_diffmode="normal"
  let g:solarized_hitrail=1
  let g:solarized_menu=0
  colorscheme solarized
endfunc
" call Solarized()
"echom $TERM $COLORTERM $TMUX

" Leader map 
let g:mapleader = " "
let g:maplocalleader = " "

" BUFFERS
" Enable changed hidden buffers! Beware of qa!
set hidden
" Close buffer but keep split
" nnoremap <leader><Backspace> :bp\|bd \#<Enter>
nnoremap <leader><Backspace> :bp<CR>:bd #<CR>
" Move through buffers
"nnoremap <leader>l :bnext<CR>
"nnoremap <leader>h :bprevious<CR> 
"nnoremap <leader>b :buffers<CR>:buffer<Space>
" hide buffer from list
" set nobuflisted
" ?
" bufhidden=hide noswapfile | r !<args>
nnoremap H ^
nnoremap L $
vnoremap <leader>y "+y
nnoremap <leader>p "*p

" TABS 
nnoremap <leader>l :tabnext<CR>
nnoremap <leader>h :tabprevious<CR> 
tnoremap <C-PageUp> t_CTRL-W_gt
tnoremap <C-PageDown> t_CTRL-W_gT
"nnoremap <alt>t :tabnew
" focus
" :tab %
" :tab ter

" MOUSE
set mouse+=a
if has("mouse_sgr")
  set ttymouse=sgr
else
  set ttymouse=xterm2
end

" UNDO
if has('persistent_undo')      " check if your vim version supports it
  set undofile                 " turn on the feature  
  silent !mkdir -p ~/.vim/undo
  set undodir=$HOME/.vim/undo  " directory where the undo files will be stored
endif

" Allow backspacing over everything in insert mode.
set backspace=indent,eol,start

" Highlight search|hls, disable temp.:nohlsearch|nohls, session:'hi clear search'
set hlsearch
nnoremap <silent><expr> <Leader>g (&hls && v:hlsearch ? ':nohls' : ':set hls')."\n"
set smartcase

" Always show # lines arround the cursor
set scrolloff=3

" Cursor location row,colum,% 
set ruler

" Line numbers 
set number
set relativenumber
" no numbers on terminal (messes width)
" autocmd TerminalOpen * setlocal nonumber norelativenumber
" autocmd BufEnter * if &buftype == 'terminal' | setlocal nonumber norelativenumber | endif
" getwininfo(win_getid())[0].terminal

" vnoremap <F7> :call term_sendkeys(term_list()[0], @")<CR>
" vnoremap <silent> <F7> :<cr> :global /<cr> :call term_sendkeys(term_list()[0], getreg('v'))<cr>

" Open new split panes to right and bottom
set splitbelow
set splitright

" Color cursor line 
"set cursorline
"augroup Colors
"	autocmd!
"	autocmd ColorScheme * highlight CursorLine   cterm=bold "ctermbg=252 ctermfg=240 gui=bold guifg=240 guibg=0
"	autocmd ColorScheme * highlight CursorLineNr cterm=bold "ctermbg=242 reverse ctermfg=240 gui=bold guifg=240 guibg=0
"	autocmd ColorScheme * highlight LineNr       cterm=none "ctermbg=232 ctermfg=240 gui=bold guifg=240 guibg=0
"augroup END

" Filetype syntax & behavior 

" Auto delete trailing whitespace before writing
" au BufWritePre *.txt,*.c,*.js,*.py %s/\s\+$//e "
" 4 spaces is tab
au Filetype markdown,bash,sh setlocal tabstop=4 softtabstop=4 shiftwidth=4 smarttab expandtab autoindent fileformat=unix foldmethod=indent

" Vimscript syntax 
augroup filetype_vim
	au!
  "au FileType vim setlocal foldmethod=marker ++once
	au FileType vim setlocal foldmethod=marker
	au Filetype vim setlocal tabstop=2 softtabstop=2 shiftwidth=2 smarttab expandtab autoindent
augroup END

" Other Mappings 
" vimgrep within files containing the word under the cursor
map <F4> :execute "noautocmd vimgrep /" . expand("<cword>") . "/j **" <Bar> cw<CR>

" git mergetools
if &diff
    map <leader>1 :diffget LOCAL<CR>
    map <leader>2 :diffget BASE<CR>
    map <leader>3 :diffget REMOTE<CR>
endif

" Plug Ins 

" tagbar 
nmap <F8> :TagbarToggle<CR>

" fzf 
" set rtp+=~/.fzf "for manual install
source /usr/share/doc/fzf/examples/fzf.vim
nnoremap <silent> <Leader>b :Buffers<CR>
nnoremap <silent> <C-f> :Files<CR>
nnoremap <silent> <Leader>f :Rg<CR>
nnoremap <silent> <Leader>/ :BLines<CR>
nnoremap <silent> <Leader>' :Marks<CR>
"nnoremap <silent> <Leader>g :Commits<CR>
"nnoremap <silent> <Leader>H :Helptags<CR>
"nnoremap <silent> <Leader>hh :History<CR>
"nnoremap <silent> <Leader>h: :History:<CR>
"nnoremap <silent> <Leader>h/ :History/<CR>
" https://github.com/junegunn/fzf/blob/master/ADVANCED.md#generating-fzf-color-theme-from-vim-color-schemes
let g:fzf_colors =
  \{'fg':         ['fg', 'Normal'],
  \ 'bg':         ['bg', 'Normal'],
  \ 'preview-bg': ['bg', 'NormalFloat'],
  \ 'hl':         ['fg', 'Comment'],
  \ 'fg+':        ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':        ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':        ['fg', 'Statement'],
  \ 'info':       ['fg', 'PreProc'],
  \ 'border':     ['fg', 'Ignore'],
  \ 'prompt':     ['fg', 'Conditional'],
  \ 'pointer':    ['fg', 'Exception'],
  \ 'marker':     ['fg', 'Keyword'],
  \ 'spinner':    ['fg', 'Label'],
  \ 'header':     ['fg', 'Comment'] }
" :echo fzf#wrap()
" :call append('$', printf('export FZF_DEFAULT_OPTS="%s"', matchstr(fzf#wrap().options, "--color[^']*")))

" figletify  the current line
" needs pyfiglet python package
"map <F3> :execute "read !pyfiglet ".getline(".")<CR>

" Terminal 

" Send to terminal 
"" open a terminal send current line or lines to it
" IPYTHON
"" use %autoindent 
" $ mkdir -p ~/.config/ipython/profile_default
" $ echo 'set TerminalInteractiveShell.autoindent=False'>>~/.config/ipython/profile_default/ipython_config.py
" TODO 
"" yank to "+, switch to term, %paste <cr>
"" wincmd p switches to previous window
function s:send_lines_to_term(lnum1, lnum2)
  " get terminal buffer
  let g:terminal_buffer = get(g:, 'terminal_buffer', -1)
  " open new terminal if it doesn't exist
  if g:terminal_buffer == -1 || !bufexists(g:terminal_buffer)
    terminal
    let g:terminal_buffer = bufnr('')
    wincmd p
  " split a new window if terminal buffer hidden
  elseif bufwinnr(g:terminal_buffer) == -1
    exec 'sbuffer ' . g:terminal_buffer
    wincmd p
  endif
  " join lines with "\<cr>", note the extra "\<cr>" for last line
  " send joined lines to terminal.
  " call term_sendkeys(g:terminal_buffer,
  "       \ join(getline(a:lnum1, a:lnum2), "\<cr>") . "\<cr>")
  let lines = filter(getline(a:lnum1, a:lnum2), '!empty(v:val)')
  call term_sendkeys(g:terminal_buffer, join(lines, "\<cr>") . "\<cr>")
  " Move cursor to lnum2
  call cursor(a:lnum2, 0) "line(a:lnum2) + col('.') - 1)
endfunction

" terminal mappins 
command! -range SendToTerm call s:send_lines_to_term(<line1>, <line2>)
nnoremap <leader>x :SendToTerm<cr>
vnoremap <leader>x :SendToTerm<cr>

vnoremap <leader>v "+y :call term_sendkeys(g:terminal_buffer, '%paste')<cr>
function s:paste_to_ipython_term(lnum1, lnum2)
  let g:terminal_buffer = get(g:, 'terminal_buffer', -1)
  if g:terminal_buffer == -1 || !bufexists(g:terminal_buffer)
    terminal
    let g:terminal_buffer = bufnr('')
    wincmd p
  elseif bufwinnr(g:terminal_buffer) == -1
    exec 'sbuffer ' . g:terminal_buffer
    call term_sendkeys(g:terminal_buffer, 'ipython')
    wincmd p
  endif
  " call term_sendkeys(g:terminal_buffer, '%paste')<cr>
  call term_sendkeys(g:terminal_buffer, '%paste')
  call term_sendkeys(g:terminal_buffer, '\<cr>')
  " Move cursor to lnum2
  call cursor(a:lnum2, 0) "line(a:lnum2) + col('.') - 1)
endfunction
command! -range PasteToIPython call paste_to_ipython_term(<line1>, <line2>)
vnoremap <leader>c :PasteToIpython<cr>

" :ter terminal mode
" <C-w><C-w> jump between windows (including terminal)
" https://vimhelp.org/terminal.txt.html#t_CTRL-W_N
" <C-w>N enter normal mode in console, i to return to insert mode 
" tnoremap <F2> <C-W>N
tnoremap <F2> <C-W>N :set nu! rnu!<CR>
tnoremap <F3> <C-W>Nyy<C-W><C-W>p
" tnoremap <F3> <C-W>Nyy<C-W>P

nnoremap <leader>n :set nu! rnu!<cr>

" figletify  the current line
" needs pyfiglet python package
"map <F3> :execute "read !pyfiglet ".getline(".")<CR>

source ~/.vim/ale.vim

" SCRAPS 
" General tab~4
" au BufNewFile,BufRead *.txt,*.py,*.c,*.h,*.js,*.jsx,*.json,*.html,*.css,*.scss setl ts=4 softtabstop=4 sw=4 et autoindent
"
" Get rid of extra whitespace on save
" au BufWritePre *.txt,*.c,*.jsx,*.js,*.py %s/\s\+$//e 
"
" Force encoding
" set encoding=utf-8
"
" General Indenting
"" use a mix of tab=4 and spaces
" set tabstop=4
" set softtabstop=4
" set shiftwidth=4
" set expandtab
" set smartindent

":command! -nargs=* -complete=shellcmd Fcmd new | setlocal buftype=nofile
"
" all loaded colorschemes get modified 
" also using AutoColors plugin 
"   .vim/colors/after/common.vim to make bg transparent
" terminal color problems
" https://github.com/tmux/tmux/issues/1246
"if has('termguicolors')
"	if exists('$TMUX')
"    let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
"    let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
"    set t_Co=256
"    set termguicolors
"    if &term =~ '256color'
"       disable Background Color Erase (BCE) so that color schemes
"       render properly when inside 256-color tmux and GNU screen.
"       see also http://snk.tuxfamily.org/log/vim-256color-bce.html
"      set t_ut=
"    endif
"  endif
"endif

