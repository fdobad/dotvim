" echom('INI ALE')
"
" python language server protocol
"
" https://github.com/python-lsp/python-lsp-server   https://github.com/python-lsp/python-lsp-server/blob/develop/CONFIGURATION.md
" https://github.com/python-lsp/pylsp-mypy
" https://github.com/python-lsp/python-lsp-black
" https://github.com/python-rope/rope               https://rope.readthedocs.io/en/latest/overview.html
"
" pip install python-lsp-server[pylint] pylsp-mypy python-lsp-black pylsp-rope
"
"
" vim linter
" https://github.com/Vimjas/vint                    pip install vim-vint
"
"
" let b:ale_python_pylsp_executable = pylsp
" let b:ale_python_pylsp_options =
" let b:ale_python_pylsp_use_global = 
let b:ale_python_pylsp_auto_pipenv = 1
let g:ale_python_pylsp_config = {
\   'pylsp': {
\     'plugins': {
\       'autopep8': {
\         'enabled': v:false,
\       },
\       'flake8': {
\         'enabled': v:false,
\       },
\       'mccabe': {
\         'enabled': v:false,
\       },
\       'pycodestyle': {
\         'enabled': v:false,
\       },
\       'pydocstyle': {
\         'enabled': v:false,
\       },
\       'pyflakes': {
\         'enabled': v:false,
\       },
\       'pylint': {
\         'enabled': v:false,
\       },
\       'rope': {
\         'enabled': v:true,
\       },
\       'ruff': {
\         'enabled': v:true,
\       },
\       'yapf': {
\         'enabled': v:false,
\       },
\       'mypy': {
\         'enabled': v:true,
\       },
\       'black': {
\         'enabled': v:true,
\       },
\     },
\   },
\}

" This Dictionary configures which linters are enabled for which languages.
let g:ale_linters = {
\   'python': ['pylsp','ruff'],
\   'vim' : ['vint'],
\   '*' : [],
\}

" This option can be changed to only enable explicitly selected linters.
let g:ale_linters_explicit = 1 " get(g:, 'ale_linters_explicit', 0)

" This Dictionary configures which functions will be used for fixing problems. 'pycln',
" 'remove_trailing_lines', 'trim_whitespace'],
let g:ale_fixers = {
\   'python': ['ruff','black', 'isort'], 
\   '*' : [],
\}

let g:ale_python_black_options = '--line-length 120'
" This Dictionary allows users to set up filetype aliases for new filetypes.
"let g:ale_linter_aliases = get(g:, 'ale_linter_aliases', {})

let g:ale_lint_delay = 333 " def 200ms

" lint_on
let g:ale_lint_on_text_changed = 'normal' ", 'never', 'always', 'insert' def:norm
let g:ale_lint_on_insert_leave = 1 " 0 def:1
let g:ale_lint_on_enter = 1 " 0 def:1
let g:ale_lint_on_save = 1 " 0 def:1
let g:ale_lint_on_filetype_changed =  1 " def:1
let g:ale_lsp_suggestions = 1 " def:0

" fix on
let g:ale_fix_on_save = 1 "def:0

let g:ale_enabled = 1 " def 1

" A Dictionary mapping linter or fixer names to Arrays of two-item Arrays
" mapping filename paths from one system to another.
"let g:ale_filename_mappings = {}

" This Dictionary configures the default project roots for various linters.
"let g:ale_root = get(g:, 'ale_root', {})

" fill loc or quick
let g:ale_set_loclist = 1 " def 1
let g:ale_set_quickfix = 0 " def 0

"let g:ale_set_signs = has('signs')
"let g:ale_set_highlights = has('syntax')
"let g:ale_exclude_highlights = []
"let g:ale_echo_cursor = 1 " def 1 echoing when the cursor moves
let g:ale_cursor_detail = 0 " def 0 automatically show errors in the preview window
let g:ale_virtualtext_cursor = 1 " def 1 v>9.0
let g:ale_hover_cursor = 1 "def 1 enable LSP hover messages at the cursor

let g:ale_close_preview_on_insert = 1 " def 0 automatically close the preview window upon entering Insert Mode
"let g:ale_set_balloons = 0 " not supported has('balloon_eval') && has('gui_running')
let g:ale_hover_to_preview = 1 " def 0

let g:ale_completion_enabled = 1     " def 0
set omnifunc=ale#completion#OmniFunc " <C-x><C-o>
let g:ale_python_auto_pipenv = 1     " def 0
let g:ale_python_auto_virtualenv = 1 " def 0

" let g:ale_popup_menu_enabled = 1 " def has('gui_running')
let b:ale_echo_msg_format = '%linter%: %severity%: %code: %%s'
let g:ale_loclist_msg_format = '%linter%: %severity%: %code: %%s' "'%code: %%s'

let g:ale_sign_info = 'I'
let g:ale_sign_warning = 'W'
let g:ale_sign_error = 'E'

function! LinterStatus() abort
  let l:counts = ale#statusline#Count(bufnr(''))
  let l:all_errors = l:counts.error + l:counts.style_error
  let l:all_non_errors = l:counts.total - l:all_errors
  return l:counts.total == 0 ? '🐛' : printf(
  \   '%d🐝 %d🐞',
  \   all_non_errors,
  \   all_errors
  \)
  " \   '%d❕ %d❗',
  " return l:counts.total == 0 ? 'OK' : printf(
endfunction
" set statusline=%<%f\ %h%m%r%=%-14.(%l,%c%V%)\ %P
set statusline=%<%f\ %h%m%r%{fugitive#statusline()}%=%-14.(%l,%c%V%)\ %P
set statusline+=%=
set statusline+=\ %{LinterStatus()}


noremap ]e <Plug>(ale_next_error)
noremap [e <Plug>(ale_previous_error)
noremap ]g <Plug>(ale_next)
noremap [g <Plug>(ale_previous)
noremap K <Plug>(ale_hover)
noremap <leader>k <Plug>(ale_find_references)
imap <C-x><C-p> <Plug>(ale_show_completion_menu)

" echom('END ALE')
